﻿using System;
using System.Collections.Generic;

namespace ProjectIntesoV1.Models
{
    public partial class BlogCategory
    {
        public BlogCategory()
        {
            Blogs = new HashSet<Blog>();
        }

        public int Id { get; set; }
        public string? Vname { get; set; }
        public string? Ename { get; set; }
        public int Status { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
    }
}
