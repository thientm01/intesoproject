﻿using System;
using System.Collections.Generic;

namespace ProjectIntesoV1.Models
{
    public partial class ProductCategory
    {
        public ProductCategory()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string? Vname { get; set; }
        public int? Status { get; set; }
        public string? Ename { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
