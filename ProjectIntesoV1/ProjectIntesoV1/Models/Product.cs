﻿using System;
using System.Collections.Generic;

namespace ProjectIntesoV1.Models
{
    public partial class Product
    {
        public Product()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int Id { get; set; }
        public string? Ename { get; set; }
        public string? Vname { get; set; }
        public string? Edescription { get; set; }
        public string? Vdescription { get; set; }
        public string? Slug { get; set; }
        public int? Status { get; set; }
        public string? Img { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? AuthorId { get; set; }
        public int? CategoryId { get; set; }
        public double? Price { get; set; }
        public int? Stock { get; set; }
        public string? TechnicalInfor { get; set; }
        
        public virtual AccountUser? Author { get; set; }
        public virtual ProductCategory? Category { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
