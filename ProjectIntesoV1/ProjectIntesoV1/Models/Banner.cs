﻿using System;
using System.Collections.Generic;

namespace ProjectIntesoV1.Models
{
    public partial class Banner
    {
        public int BannerId { get; set; }
        public string? Title { get; set; }
        public string? UrlFile { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? SortOrder { get; set; }
    }
}
