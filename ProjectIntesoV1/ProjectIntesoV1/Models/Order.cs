﻿using System;
using System.Collections.Generic;

namespace ProjectIntesoV1.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int Id { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Status { get; set; }
        public string? CustomerName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public bool? IsRedBill { get; set; }
        public double? Total { get; set; }
        public int? AccountId { get; set; }

        public virtual AccountUser? Account { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
