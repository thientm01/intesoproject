﻿using System;
using System.Collections.Generic;

namespace ProjectIntesoV1.Models
{
    public partial class Contact
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string? Email { get; set; }
        public string? Name { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public string? MessContent { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
