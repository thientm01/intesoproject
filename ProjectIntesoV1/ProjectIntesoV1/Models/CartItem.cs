﻿namespace ProjectIntesoV1.Models
{
    public class CartItem
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public float subTotal { get; set; }
        public CartItem()
        {
        }
        public CartItem(Product product, int quantity)
        {
            Product = product;
            Quantity = quantity;
            subTotal = (float)(quantity * product.Price);
        }
    }
}
