﻿using System;
using System.Collections.Generic;

namespace ProjectIntesoV1.Models;

public partial class Blog
{
    public int BlogId { get; set; }

    public string? Title { get; set; }

    public string? EngTitle { get; set; }

    public int? CategoryId { get; set; }

    public string? Vcontent { get; set; }

    public string? Econtent { get; set; }

    public DateTime? PublishDate { get; set; }

    public DateTime? CreateDate { get; set; }

    public string? Slug { get; set; }

    public int? NumberOfViews { get; set; }

    public string? Vsummary { get; set; }

    public string? Esummary { get; set; }

    public int? AuthorId { get; set; }

    public string? Img { get; set; }

    public bool? Status { get; set; }

    public virtual AccountUser? Author { get; set; }

    public virtual BlogCategory? Category { get; set; }
}
