﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectIntesoV1.Models
{
    public partial class Image
    {
        public int Id { get; set; }
        [Required]
        public string? Title { get; set; }
        [Required]
        public string? UrlFile { get; set; }

        public string? CloudId { get; set; }
        
    }
}
