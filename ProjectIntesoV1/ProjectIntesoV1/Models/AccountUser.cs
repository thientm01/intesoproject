﻿using System;
using System.Collections.Generic;

namespace ProjectIntesoV1.Models
{
    public partial class AccountUser
    {
        public AccountUser()
        {
            Blogs = new HashSet<Blog>();
            Orders = new HashSet<Order>();
            Products = new HashSet<Product>();
        }

        public int AccountId { get; set; }
        public string? Username { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public string? Status { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? Role { get; set; }
        public string? FullName { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
