﻿using Newtonsoft.Json;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Services
{
    public class ShoppingCartService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ShoppingCartService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private List<CartItem> GetCartItemsFromSession()
        {
            var session = _httpContextAccessor.HttpContext.Session;
            var cartJson = session.GetString("Cart");
            if (cartJson != null)
            {
                return JsonConvert.DeserializeObject<List<CartItem>>(cartJson);
            }
            return new List<CartItem>();
        }

        private void SaveCartItemsToSession(List<CartItem> cartItems)
        {
            var session = _httpContextAccessor.HttpContext.Session;
            session.SetString("Cart", JsonConvert.SerializeObject(cartItems));
        }

        public List<CartItem> GetCartItems()
        {
            return GetCartItemsFromSession();
        }

        public void AddItemToCart(Product product, int quantity)
        {
            var cartItems = GetCartItemsFromSession();
            var existingItem = cartItems.FirstOrDefault(item => item.Product.Id == product.Id);
            if (existingItem != null)
            {
                existingItem.Quantity += quantity;
                existingItem.subTotal = (float)(existingItem.Quantity * product.Price);
            }
            else
            {
                cartItems.Add(new CartItem(product, quantity));
            }
            SaveCartItemsToSession(cartItems);
        }

        public void UpdateCartItem(int productId, int quantity)
        {
            var cartItems = GetCartItemsFromSession();
            var existingItem = cartItems.FirstOrDefault(item => item.Product.Id == productId);
            if (existingItem != null)
            {
                existingItem.Quantity = quantity;
                existingItem.subTotal = (float)(existingItem.Quantity * existingItem.Product.Price);
                SaveCartItemsToSession(cartItems);
            }
        }

        public void RemoveItemFromCart(int productId)
        {
            var cartItems = GetCartItemsFromSession();
            var itemToRemove = cartItems.FirstOrDefault(item => item.Product.Id == productId);
            if (itemToRemove != null)
            {
                cartItems.Remove(itemToRemove);
                SaveCartItemsToSession(cartItems);
            }
        }

        public void ClearCart()
        {
            SaveCartItemsToSession(new List<CartItem>());
        }
    }
}
