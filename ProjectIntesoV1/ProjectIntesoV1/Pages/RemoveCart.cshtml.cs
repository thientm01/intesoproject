﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectIntesoV1.Models;
using ProjectIntesoV1.Services;

namespace ProjectIntesoV1.Pages
{
    public class RemoveCartModel : PageModel
    {
        private readonly ShoppingCartService _shoppingCartService;
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public RemoveCartModel(ProjectIntesoV1.Models.IntesoWebContext context, ShoppingCartService shoppingCartService)
        {
            _context = context;
            _shoppingCartService = shoppingCartService;
        }

        public IActionResult OnGet(int productId)
        {
            var product = _context.Products.FirstOrDefault(p => p.Id == productId);

            _shoppingCartService.RemoveItemFromCart(productId);
            return RedirectToPage("CartController");
        }

    }
}
