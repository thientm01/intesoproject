using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages
{
    public class BlogDetailModel : PageModel
    {

        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public BlogDetailModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }
        public Blog blog { get; set; }
        public void OnGet(int? pid)
        {

            blog = _context.Blogs.Include(x => x.Category).Include(x => x.Author).FirstOrDefault(x => x.BlogId == pid);
            blog.NumberOfViews = blog.NumberOfViews + 1;
            _context.Update(blog);
            _context.SaveChanges();

        }
    }
}
