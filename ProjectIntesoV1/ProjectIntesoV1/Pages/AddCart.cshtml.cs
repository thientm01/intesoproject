﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Services;
using System;

namespace ProjectIntesoV1.Pages
{
    public class AddCartModel : PageModel
    {
        private readonly ShoppingCartService _shoppingCartService;
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public AddCartModel(ProjectIntesoV1.Models.IntesoWebContext context, ShoppingCartService shoppingCartService)
        {
            _context = context;
            _shoppingCartService = shoppingCartService;

        }
        public IActionResult OnGet(int productId, int quantity, string action)
        {
            var product = _context.Products.FirstOrDefault(p => p.Id == productId);

            if (action == "add")
            {
                if (product != null)
                {
                    _shoppingCartService.AddItemToCart(product, quantity);
                    return new JsonResult(new { success = true, message = "Bạn đã thêm sản phẩm " + product.Vname + " vào giỏ hàng thành công!" });

                }

            }
            if (action == "edit")
            {
                _shoppingCartService.UpdateCartItem(productId, quantity);
                return new JsonResult(new { success = true, message = "Bạn cập nhật  sản phẩm " + product.Vname + " trong giỏ hàng thành công!" });
            }
            return new JsonResult(new { success = false, message = "Sản phẩm không tồn tại" });
        }

        public IActionResult OnPost(int productId, int quantity, int action)
        {
            if (action == 1)
            {
                var product = _context.Products.FirstOrDefault(p => p.Id == productId);
                if (product != null)
                {
                    _shoppingCartService.AddItemToCart(product, quantity);
                }
            }
            if (action == 1)
            {
                _shoppingCartService.UpdateCartItem(productId, quantity);

            }
            return new JsonResult(new { success = true, message = "Item added to cart successfully." });
        }
    }
}
