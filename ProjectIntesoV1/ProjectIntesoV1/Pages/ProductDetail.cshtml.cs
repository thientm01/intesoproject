using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages
{
    public class ProductDetailModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public ProductDetailModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }
        public Product product { get; set; }
        public void OnGet(int? pid)
        {
            product = _context.Products.Include(x => x.Category).FirstOrDefault(x => x.Id == pid);

        }
    }
}
