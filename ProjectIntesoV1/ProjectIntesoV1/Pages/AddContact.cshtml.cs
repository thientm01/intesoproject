using CloudinaryDotNet.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages
{
    public class AddContactModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public AddContactModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }
        public IActionResult OnGet()
        {
            return Page();
        }

        public IActionResult OnPost(ContactRequest r)
        {

            Contact contact = new Contact();
            contact.Name = r.name;
            contact.Email = r.email;
            contact.Phone = r.phone;
            contact.Address = r.address;
            contact.Title = r.subject;
            contact.MessContent = r.message;
            contact.CreateDate = DateTime.Now;
            contact.Status = 1;
            _context.Contacts.Add(contact);
            _context.SaveChanges();
            return RedirectToPage("Index");
        }
       public  class ContactRequest
        {
            public string name { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string address { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
        }
    }
    
}
