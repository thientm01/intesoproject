using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages
{
    public class BlogListModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public BlogListModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        public IList<Blog> DisplayedBlog { get; set; }
        public IList<BlogCategory> Categories { get; set; }
        public int TotalPages { get; set; }
        public int curentPage { get; set; }

        public IActionResult OnGet(string? searchString, int? categoryId, string? sortOrder, string? currentIndex)
        {
            IQueryable<Blog> products = _context.Blogs.Include(p => p.Category).Where(x=>x.Status ==true);

            if (!string.IsNullOrEmpty(searchString))
            {
                products = products.Where(p => p.EngTitle.Contains(searchString) || p.Title.Contains(searchString));
            }

            if (categoryId.HasValue)
            {
                products = products.Where(p => p.CategoryId == categoryId);
            }

            switch (sortOrder)
            {
                case "price_asc":
                    products = products.OrderBy(p => p.CreateDate);
                    break;
                case "price_desc":
                    products = products.OrderByDescending(p => p.CreateDate);
                    break;
                default:
                    products = products.OrderBy(p => p.BlogId);
                    break;
            }

            // Paging
            const int pageSize = 9;
            TotalPages = (int)Math.Ceiling(products.Count() / (double)pageSize);
            int pageNumber = 1;
            if (!string.IsNullOrEmpty(currentIndex))
            {
                pageNumber = int.Parse(currentIndex);
            }

            DisplayedBlog = products.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            // Fetching categories
            Categories = _context.BlogCategories.ToList();
            ViewData["page"] = pageNumber;
            ViewData["searchString"] = searchString == null ? "" : searchString;
            ViewData["sortOrder"] = sortOrder == null ? "" : sortOrder;
            ViewData["categoryId"] = categoryId == null ? "" : categoryId;
            curentPage = pageNumber;
            return Page();
        }
    }
}
