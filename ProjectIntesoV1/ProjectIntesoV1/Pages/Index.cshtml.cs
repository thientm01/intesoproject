﻿using BSS;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;
using static ProjectIntesoV1.Pages.AddContactModel;

namespace ProjectIntesoV1.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public IndexModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }
        public IList<Product> DisplayedProducts { get; set; }
        [BindProperty]
        public ProjectIntesoV1.Models.Contact Contact { get; set; }

        public List<Banner> listBanners = new List<Banner>();
        public Blog IntroBlog { get; set; }
        public void OnGet()
        {
            listBanners = _context.Banners.Take(3).OrderBy(b => b.SortOrder).ToList();
            DisplayedProducts =_context.Products.Include(x=>x.Category).Take(4).OrderBy(b => b.Id).ToList();
            foreach (Banner banner in listBanners)
            {
                banner.UrlFile = Common.ScaleDownImage(banner.UrlFile, 1800, 900);
            }
            IntroBlog = _context.Blogs.FirstOrDefault(x => x.BlogId == 17);
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Contact.Status = 1;
            Contact.CreateDate = DateTime.Now;

            _context.Contacts.Add(Contact);
            _context.SaveChanges();

            return RedirectToPage("/Index"); // Redirect to index page
        }
    }
}