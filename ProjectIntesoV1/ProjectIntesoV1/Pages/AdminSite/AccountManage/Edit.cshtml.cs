﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.AccountManage
{
    public class EditModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public EditModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        [BindProperty]
        public AccountUser AccountUser { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AccountUser = await _context.AccountUsers.FirstOrDefaultAsync(m => m.AccountId == id);

            if (AccountUser == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(AccountUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountUserExists(AccountUser.AccountId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool AccountUserExists(int id)
        {
            return _context.AccountUsers.Any(e => e.AccountId == id);
        }
    }
}
