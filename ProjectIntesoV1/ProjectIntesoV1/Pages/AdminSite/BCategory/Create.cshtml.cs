﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.BCategory
{
    public class CreateModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public CreateModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public BlogCategory BlogCategory { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid || _context.BlogCategories == null || BlogCategory == null)
            {
                return Page();
            }

            _context.BlogCategories.Add(BlogCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
