﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.BCategory
{
    public class DeleteModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public DeleteModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        [BindProperty]
      public BlogCategory BlogCategory { get; set; } = default!;

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.BlogCategories == null)
            {
                return NotFound();
            }

            var blogcategory = await _context.BlogCategories.FirstOrDefaultAsync(m => m.Id == id);

            if (blogcategory == null)
            {
                return NotFound();
            }
            else 
            {
                BlogCategory = blogcategory;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null || _context.BlogCategories == null)
            {
                return NotFound();
            }
            var blogcategory = await _context.BlogCategories.FindAsync(id);

            if (blogcategory != null)
            {
                BlogCategory = blogcategory;
                _context.BlogCategories.Remove(BlogCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
