﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.PCategory
{
    public class DetailsModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public DetailsModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

      public ProductCategory ProductCategory { get; set; } = default!; 

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null || _context.ProductCategories == null)
            {
                return NotFound();
            }

            var productcategory = await _context.ProductCategories.FirstOrDefaultAsync(m => m.Id == id);
            if (productcategory == null)
            {
                return NotFound();
            }
            else 
            {
                ProductCategory = productcategory;
            }
            return Page();
        }
    }
}
