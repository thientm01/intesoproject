﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.BlogManage
{
    public class EditModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public EditModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Blog Blog { get; set; }


        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Blog = await _context.Blogs
                .Include(b => b.Author)
                .Include(b => b.Category).FirstOrDefaultAsync(m => m.BlogId == id);

            if (Blog == null)
            {
                return NotFound();
            }
           ViewData["AuthorId"] = new SelectList(_context.AccountUsers, "AccountId", "AccountId");
           ViewData["CategoryId"] = new SelectList(_context.BlogCategories, "Id", "Vname");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Blog).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogExists(Blog.BlogId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool BlogExists(int id)
        {
            return _context.Blogs.Any(e => e.BlogId == id);
        }
    }
}
