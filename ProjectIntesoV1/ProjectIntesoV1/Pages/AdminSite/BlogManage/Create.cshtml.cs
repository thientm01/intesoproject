﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.BlogManage
{
    public class CreateModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public CreateModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["AuthorId"] = new SelectList(_context.AccountUsers, "AccountId", "AccountId");
        ViewData["CategoryId"] = new SelectList(_context.BlogCategories, "Id", "Vname");
            return Page();
        }

        [BindProperty]
        public Blog Blog { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            Blog.AuthorId = 1;
            Blog.Status = true;
            Blog.CreateDate = DateTime.Now;
            Blog.PublishDate  = DateTime.Now.AddDays(1);
            _context.Blogs.Add(Blog);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
