﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.BannerManage
{
    public class DetailsModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public DetailsModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        public Banner Banner { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Banner = await _context.Banners.FirstOrDefaultAsync(m => m.BannerId == id);

            if (Banner == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
