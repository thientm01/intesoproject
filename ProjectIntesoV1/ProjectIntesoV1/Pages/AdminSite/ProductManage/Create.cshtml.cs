﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.ProductManage
{
    public class CreateModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public CreateModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["AuthorId"] = new SelectList(_context.AccountUsers, "AccountId", "AccountId");
        ViewData["CategoryId"] = new SelectList(_context.ProductCategories, "Id", "Vname");
            return Page();
        }

        [BindProperty]
        public Product Product { get; set; } = default!;
        

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
          if (!ModelState.IsValid || _context.Products == null || Product == null)
            {
                return Page();
            }
            Product.AuthorId = 1;
            Product.CreateDate = DateTime.Now;

            _context.Products.Add(Product);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
