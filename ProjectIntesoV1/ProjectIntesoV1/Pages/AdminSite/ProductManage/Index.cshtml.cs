﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.ProductManage
{
    public class IndexModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public IndexModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        public IList<Product> Product { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Products != null)
            {
                Product = await _context.Products
                .Include(p => p.Author)
                .Include(p => p.Category).ToListAsync();
            }
        }
    }
}
