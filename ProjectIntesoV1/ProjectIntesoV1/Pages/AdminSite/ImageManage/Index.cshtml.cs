﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.ImageManage
{
    public class IndexModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public IndexModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        public IList<Image> Image { get;set; }

        public async Task OnGetAsync()
        {
            Image = await _context.Images.ToListAsync();
        }
    }
}
