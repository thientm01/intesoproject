﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.ImageManage
{
    public class DeleteModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public DeleteModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Image Image { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Image = await _context.Images.FirstOrDefaultAsync(m => m.Id == id);

            if (Image == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Image = await _context.Images.FindAsync(id);

            Account account = new Account(
            "dplelrpij",
            "355177995885353",
            "5yPencgEPTHzqRLSeRzy5vzCTOo");
            Cloudinary cloudinary = new Cloudinary(account);
            string imageId = Image.CloudId;
            DeletionParams deletionParams = new DeletionParams(imageId);

            DeletionResult result = cloudinary.Destroy(deletionParams);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                if (Image != null)
                {
                    _context.Images.Remove(Image);
                    await _context.SaveChangesAsync();
                }
            }

            return RedirectToPage("./Index");
        }
    }
}
