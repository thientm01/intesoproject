﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProjectIntesoV1.DTO;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages.AdminSite.ImageManage
{
    public class CreateModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        private readonly Cloudinary _cloudinary;
     
        public UploadResult UploadResult { get; private set; }

        public CreateModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
            Account account = new Account(
              "dplelrpij",
              "355177995885353",
              "5yPencgEPTHzqRLSeRzy5vzCTOo");
            _cloudinary = new Cloudinary(account);
        }


        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public ImageDTO Img { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            if (Img.ImageFile != null && Img.ImageFile.Length > 0)
            {
                using (var stream = Img.ImageFile.OpenReadStream())
                {
                    var uploadParams = new ImageUploadParams
                    {
                        File = new FileDescription(Img.ImageFile.FileName, stream)
                    };

                    UploadResult = await _cloudinary.UploadAsync(uploadParams);
                }
            }
            Image Image = new Image();
            Image.UrlFile = UploadResult.Url.ToString();
            Image.Title = Img.Title;
            Image.CloudId = UploadResult.PublicId;

            _context.Images.Add(Image);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
