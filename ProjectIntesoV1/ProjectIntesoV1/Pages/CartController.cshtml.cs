using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ProjectIntesoV1.Models;
using ProjectIntesoV1.Services;

namespace ProjectIntesoV1.Pages
{
    public class CartControllerModel : PageModel
    {
        private readonly ShoppingCartService _shoppingCartService;
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public CartControllerModel(ProjectIntesoV1.Models.IntesoWebContext context, ShoppingCartService shoppingCartService)
        {
            _context = context;
            _shoppingCartService = shoppingCartService;

        }
        public List<CartItem> CartItems { get; set; }
        public float TotalBill { get; set; }

        public void OnGet()
        {
            CartItems = _shoppingCartService.GetCartItems().ToList();
        }

  

    }
}
