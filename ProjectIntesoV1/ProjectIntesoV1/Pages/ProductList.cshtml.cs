using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ProjectIntesoV1.Models;

namespace ProjectIntesoV1.Pages
{
    public class ProductListModel : PageModel
    {
        private readonly ProjectIntesoV1.Models.IntesoWebContext _context;

        public ProductListModel(ProjectIntesoV1.Models.IntesoWebContext context)
        {
            _context = context;
        }

        public IList<Product> DisplayedProducts { get; set; }
        public IList<ProductCategory> Categories { get; set; }
        public int TotalPages { get; set; }
        public int curentPage { get; set; }

        public IActionResult OnGet(string? searchString, int? categoryId, string? sortOrder, string? currentIndex)
        {
            IQueryable<Product> products = _context.Products.Include(p => p.Category);

            if (!string.IsNullOrEmpty(searchString))
            {
                products = products.Where(p => p.Ename.Contains(searchString) || p.Vname.Contains(searchString));
            }

            if (categoryId.HasValue)
            {
                products = products.Where(p => p.CategoryId == categoryId);
            }

            switch (sortOrder)
            {
                case "price_asc":
                    products = products.OrderBy(p => p.Price);
                    break;
                case "price_desc":
                    products = products.OrderByDescending(p => p.Price);
                    break;
                default:
                    products = products.OrderBy(p => p.Id);
                    break;
            }

            // Paging
            const int pageSize = 8;
            TotalPages = (int)Math.Ceiling(products.Count() / (double)pageSize);
            int pageNumber = 1;
            if (!string.IsNullOrEmpty(currentIndex))
            {
                pageNumber = int.Parse(currentIndex);
            }

            DisplayedProducts = products.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            // Fetching categories
            Categories = _context.ProductCategories.ToList();
            ViewData["page"] = pageNumber;
            ViewData["searchString"] = searchString == null ? "" : searchString;
            ViewData["sortOrder"] = sortOrder == null ? "" : sortOrder;
            ViewData["categoryId"] = categoryId == null ? "" : categoryId;
            curentPage = pageNumber;
            return Page();
        }
    }
}
