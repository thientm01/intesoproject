﻿public class Constant
{
    public static readonly IList<string> FILE_EXTENDS = new List<string> { "png", "jpg", "jpeg", "svg", "pdf", "docx", "xlsx", "doc", "xls", "zip", "rar", "webp" }.AsReadOnly();
    public static readonly IList<string> VIDEO_EXTENDS = new List<string> { "mp4", "avi", "mov" }.AsReadOnly();
    public static readonly IList<string> IMAGE_EXTENDS = new List<string> { "png", "jpg", "jpeg", "svg", "webp" }.AsReadOnly();
    public static readonly IList<string> DOCUMENT_EXTENDS = new List<string> { "pdf", "docx", "xlsx", "doc", "xls" }.AsReadOnly();
    public static readonly IList<string> COMPRESSION_EXTENDS = new List<string> { "zip", "rar" }.AsReadOnly();

    public const string AVATAR_DEFALUT = "https://toigingiuvedep.vn/wp-content/uploads/2022/01/avatar-cute-khung-long-600x600.jpg";
    public const int MAX_COUNT_VERIFY_CODE = 5; //Số lần nhập mã xác thực tối đa
    public const string USER_ADMINISTRATOR_DEFAULT = "4b4631f2-1b61-43a5-8aac-29970592ab8d";

    public static readonly Guid CHINH_SACH_CHUNG_DEFAULT = new Guid("9677fefb-72d9-4e49-a9a9-687641f67846");
}

public class Status
{
    public const int Active = 0;
    public const int InActive = 1;
}
public class OrderStatus
{
    public const int PendingConfirm = 1;
    public const int PedingSign = 2;
    public const int Cancel = 3;
    public const int Done = 4;
    public const int Reviewed = 5;

}

public class PostStatus
{
    public const int ChoDang = 1;
    public const int DangDang = 2;
    public const int DaGo = 3;
}

public class ProductStatus
{
    public const int DangBan = 1;
    public const int DaNgung = 2;
}

public class AccountStatus
{
    public const int pending = 1;
    public const int verified = 2;
    public const int cancel = 3;
}

