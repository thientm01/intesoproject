﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;

namespace BSS
{
    public static class Common
    {
        private const int defaultWidth = 800;

        public static bool IsDebugging;


        public static int ToInt(this object obj, int defaultvalue = 0)
        {
            try
            {
                return Convert.ToInt32(obj);
            }
            catch
            {
                return defaultvalue;
            }
        }

        /// <summary>
        /// Loại bỏ khoảng trắng thừa trái phải giữa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string NormalizeString(string input)
        {
            if (input == null)
            {
                return "";

            }
            // Chia chuỗi thành mảng các từ, loại bỏ khoảng trắng
            var words = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // Kết hợp các từ lại với nhau, ngăn cách bởi một khoảng trắng
            string result = String.Join(" ", words);

            return result;
        }
        public static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();
            char[] randomString = new char[length];

            for (int i = 0; i < length; i++)
            {
                randomString[i] = chars[random.Next(chars.Length)];
            }

            return new string(randomString);
        }

        public static bool IsGuidEmpty(this Guid guid)
        {
            return guid == Guid.Empty;
        }

        /// <summary>
        /// Get DateTime.Now of Vietnam region
        /// </summary>
        /// <returns></returns>
        public static DateTime DateTimeNow()
        {
            TimeZoneInfo vietnamTimeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, vietnamTimeZone);
        }
        private static string DoExGetValue(object o)
        {
            try
            {
                if (o is ConstantExpression)
                {
                    return ((ConstantExpression)o).Value!.ToString();
                }

                if (o is MethodCallExpression)
                {
                    MethodCallExpression methodCallExpression = o as MethodCallExpression;
                    MethodInfo method = methodCallExpression.Method;
                    string text = method.DeclaringType!.FullName + "." + method.Name;
                    string text2 = "";
                    foreach (Expression argument in methodCallExpression.Arguments)
                    {
                        text2 = text2 + DoExGetValue(argument) + "; ";
                    }

                    text2 = ((text2.Length != 0) ? text2.TrimEnd(';', ' ') : "?");
                    if (method.IsDefined(typeof(ExtensionAttribute), inherit: false))
                    {
                        return text2 + "." + text + "()";
                    }

                    return text + "(" + text2 + ")";
                }

                if (o is NewExpression)
                {
                    ReadOnlyCollection<Expression> arguments = ((NewExpression)o).Arguments;
                    string text3 = "";
                    for (int i = 0; i < arguments.Count; i++)
                    {
                        MemberInfo memberInfo = ((NewExpression)o).Members![i];
                        string text4 = DoExGetValue(arguments[i]);
                        text3 = ((text4.IndexOf("=") >= 0) ? (text3 + text4 + ", ") : (text3 + memberInfo.Name + " = " + text4 + ", "));
                    }

                    return "new { " + text3.TrimEnd(',', ' ') + " }";
                }

                if (o is MemberExpression)
                {
                    MemberExpression memberExpression = o as MemberExpression;
                    UnaryExpression body = Expression.Convert(memberExpression, typeof(object));
                    Expression<Func<object>> expression = Expression.Lambda<Func<object>>(body, Array.Empty<ParameterExpression>());
                    Func<object> func = expression.Compile();
                    object obj = func() ?? "null";
                    if (memberExpression.Member is FieldInfo)
                    {
                        return memberExpression.Member.Name + " = " + obj.ToString();
                    }

                    return obj.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

            return "?";
        }

        public static string GetAppSettings(string key, out string result)
        {
            result = "";
            try
            {
                result = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build()[key];
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "";
        }

        public static string GetAppSettings(string key)
        {
            return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build()[key];
        }
        public static string GetAppSettings<T>(string key, out T result)
        {
            result = default(T);
            try
            {
                result = JObject.Parse(File.ReadAllText("appsettings.json"))[key].ToObject<T>();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "";
        }

        public static T GetAppSettings<T>(string key)
        {
            return JObject.Parse(File.ReadAllText("appsettings.json"))[key].ToObject<T>();
        }

        public static string GetClientIpAddress(HttpContext _httpContext)
        {
            string result = _httpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ?? "";
            if (string.IsNullOrEmpty(result)) result = _httpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            return result;
        }

        public static string GetUserAgent(HttpContext _httpContext)
        {
            string userAgent = _httpContext.Request.Headers["User-Agent"].ToString();
            return userAgent;
        }

        public static bool IsValid(this object obj, out List<ValidationResult> results)
        {
            results = new List<ValidationResult>();
            if (obj == null) return false;
            return Validator.TryValidateObject(obj, new ValidationContext(obj), results, true);
        }
    
        // Lấy ra tất cả các fields không được đánh dấu [JsonIgnore]
        private static IEnumerable<PropertyInfo> GetPropertiesWithAttribute<TType, TAttribute>()
        {
            Func<PropertyInfo, bool> matching =
                    property => !property.GetCustomAttributes(typeof(TAttribute), false)
                                        .Any();

            return typeof(TType).GetProperties().Where(matching);
        }
        /// <summary>
        /// Lấy ra giá trị của 1 field trong object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="propertyName"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static string GetFieldValue(object obj, string propertyName, out object result)
        {
            result = null;
            try
            {
                result = obj.GetType().GetProperty(propertyName)?.GetValue(obj, null);
                return "";
            }
            catch (Exception ex) { return $"Không thể lấy giá trị của field. {ex.Message}"; }
        }

        public static string ScaleDownImage(string originalUrl, int witdh, int height)
        {
            if (originalUrl == null) return "";

            UriBuilder uriBuilder = new UriBuilder(originalUrl);

            string[] pathSegments = uriBuilder.Path.Split('/');
            string versionSegment = pathSegments[pathSegments.Length - 2];
            string imageName = pathSegments[pathSegments.Length - 1];

            uriBuilder.Path = uriBuilder.Path.Replace("/" + versionSegment + "/" + imageName, "");
            uriBuilder.Path += "/c_crop,h_"+height+",w_" + witdh + "/" + versionSegment + "/" + imageName;

            string modifiedUrl = uriBuilder.Uri.ToString();

            return modifiedUrl;
        }

    }
}
