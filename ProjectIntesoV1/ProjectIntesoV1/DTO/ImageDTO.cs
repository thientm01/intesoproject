﻿using System.ComponentModel.DataAnnotations;

namespace ProjectIntesoV1.DTO
{
    public class ImageDTO
    {
        [Required]
        public string? Title { get; set; }
        public IFormFile ImageFile { get; set; }

    }
}
