USE [master]
GO
/****** Object:  Database [IntesoWeb]    Script Date: 28/03/2024 4:16:24 CH ******/
CREATE DATABASE [IntesoWeb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'IntesoWeb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER2014\MSSQL\DATA\IntesoWeb.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'IntesoWeb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER2014\MSSQL\DATA\IntesoWeb_log.ldf' , SIZE = 816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [IntesoWeb] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [IntesoWeb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [IntesoWeb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [IntesoWeb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [IntesoWeb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [IntesoWeb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [IntesoWeb] SET ARITHABORT OFF 
GO
ALTER DATABASE [IntesoWeb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [IntesoWeb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [IntesoWeb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [IntesoWeb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [IntesoWeb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [IntesoWeb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [IntesoWeb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [IntesoWeb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [IntesoWeb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [IntesoWeb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [IntesoWeb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [IntesoWeb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [IntesoWeb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [IntesoWeb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [IntesoWeb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [IntesoWeb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [IntesoWeb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [IntesoWeb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [IntesoWeb] SET  MULTI_USER 
GO
ALTER DATABASE [IntesoWeb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [IntesoWeb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [IntesoWeb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [IntesoWeb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [IntesoWeb] SET DELAYED_DURABILITY = DISABLED 
GO
USE [IntesoWeb]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 28/03/2024 4:16:25 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL,
	[Role] [int] NULL,
	[FullName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Banner]    Script Date: 28/03/2024 4:16:25 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Banner](
	[BannerId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[UrlFile] [ntext] NULL,
	[Status] [int] NULL,
	[CreateDate] [datetime] NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[BannerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 28/03/2024 4:16:25 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[BlogId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[EngTitle] [nvarchar](50) NULL,
	[CategoryId] [int] NULL,
	[VContent] [ntext] NULL,
	[EContent] [ntext] NULL,
	[PublishDate] [datetime] NULL,
	[CreateDate] [datetime] NULL,
	[Slug] [nvarchar](250) NULL,
	[NumberOfViews] [int] NULL,
	[VSummary] [nvarchar](50) NULL,
	[ESummary] [nvarchar](50) NULL,
	[AuthorId] [int] NULL,
	[Img] [ntext] NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[BlogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlogCategory]    Script Date: 28/03/2024 4:16:25 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VName] [nvarchar](50) NULL,
	[EName] [nvarchar](50) NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_BlogCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 28/03/2024 4:16:25 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[MessContent] [ntext] NULL,
	[Status] [int] NULL,
	[CreateDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 28/03/2024 4:16:25 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EName] [nvarchar](50) NULL,
	[VName] [nvarchar](50) NULL,
	[EDescription] [ntext] NULL,
	[VDescription] [ntext] NULL,
	[Slug] [nvarchar](250) NULL,
	[Status] [int] NULL,
	[Img] [ntext] NULL,
	[CreateDate] [datetime] NULL,
	[AuthorId] [int] NULL,
	[CategoryId] [int] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 28/03/2024 4:16:25 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VName] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[EName] [nvarchar](50) NULL,
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_Account] FOREIGN KEY([AuthorId])
REFERENCES [dbo].[Account] ([AccountId])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_Account]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_BlogCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[BlogCategory] ([Id])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_BlogCategory]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Account] FOREIGN KEY([AuthorId])
REFERENCES [dbo].[Account] ([AccountId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Account]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_ProductCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ProductCategory] ([Id])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_ProductCategory]
GO
USE [master]
GO
ALTER DATABASE [IntesoWeb] SET  READ_WRITE 
GO
